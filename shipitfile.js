module.exports = function (shipit) {
    require('shipit-deploy')(shipit);

    shipit.initConfig({
        default: {
            workspace: 'C:/TEMP/nodejs-expresstest',
            deployTo: 'C:/TEMP/nodejs-expresstest/install',
            repositoryUrl: 'https://github.com/andreasfelix/nodejs-expresstest.git',
            ignores: ['.git', 'node_modules'],
            rsync: ['--del'],
            keepReleases: 2,
            key: 'C:/dev/projects/github.txt',
            shallowClone: true
        },
        staging: {
            servers: 'localhost'
        }
    });
};